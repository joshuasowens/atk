# Ask the Kitchen 

> :warning: Under Construction 

<p>
  <a href="https://choosealicense.com/licenses/mit/" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

An inventory management system for your kitchen. Ever gone to the store and forget if you have enough garlic powder at home to make dinner that night? You never have to remember again, just ask the kitcken!

## Dependencies

- [Docker](https://docs.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

## Installation


## Usage


## Contributing

