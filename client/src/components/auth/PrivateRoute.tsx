import React, { useContext, ReactNode } from "react";
import { Route, Redirect } from "react-router-dom";
import { UserContext } from "../../context";

interface Props {
  children: ReactNode;
  exact?: boolean;
  path?: string;
}

export default function PrivateRoute({ children, exact = false, path }: Props) {
  const { user } = useContext(UserContext);
  const redirect = "/login";
  let isAuthenicated = false;

  if (localStorage.getItem("token") && user) {
    isAuthenicated = true;
  }

  return (
    <Route
      exact={exact}
      path={path}
      render={({ location }) =>
        isAuthenicated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: redirect,
              search: `?next=${location.pathname}`,
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}