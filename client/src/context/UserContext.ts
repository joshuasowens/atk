import { createContext } from "react";

type ContextProps = { 
  user: Object | null,
  setUser: (user: Object) => void
};

export const UserContext = createContext<ContextProps>({
  user: {},
  setUser: function () {}
});
