import React, { useState } from "react";
import {
  Box,
  Button,
  Collapsible,
  Heading,
  Grommet,
  Layer,
  ResponsiveContext,
} from "grommet";
import { FormClose, Notification } from "grommet-icons";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ApolloProvider } from "@apollo/react-hooks";
import { split, HttpLink, ApolloClient, InMemoryCache } from "@apollo/client";
import { getMainDefinition } from "@apollo/client/utilities";
import { setContext } from "@apollo/client/link/context";
import { PrivateRoute } from "./components/auth";
import { UserContext } from "./context";

const httpLink = new HttpLink({
  uri: "http://localhost:8000/graphql",
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem("token");
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `JWT ${token}` : "",
    },
  };
});

const splitLink = split(({ query }) => {
  const definition = getMainDefinition(query);
  return (
    definition.kind === "OperationDefinition" &&
    definition.operation === "subscription"
  );
}, httpLink);

const theme = {
  global: {
    colors: {
      brand: "#228BE6",
    },
    font: {
      family: "Roboto",
      size: "14px",
      height: "20px",
    },
  },
};


const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: authLink.concat(splitLink as any) as any,
  defaultOptions: {
    watchQuery: {
      fetchPolicy: "no-cache",
      errorPolicy: "ignore",
    },
    query: {
      fetchPolicy: "no-cache",
      errorPolicy: "all",
    },
  },
});

const AppBar = (props: Object) => (
  <Box
    tag="header"
    direction="row"
    align="center"
    justify="between"
    background="brand"
    pad={{ left: "medium", right: "small", vertical: "small" }}
    elevation="medium"
    style={{ zIndex: 1 }}
    {...props}
  />
);

function Home() {
  return (
    <Box flex align="center" justify="center">
      app body
    </Box>
  );
}

function App() {
  const storedUserString: string = localStorage.getItem("user") || "{}";
  let storedUser: Object | null = null;

  if (storedUserString) {
    storedUser = JSON.parse(storedUserString);
  }

  const [user, updateUser] = useState(storedUser);
  const [showSidebar, setShowSideBar] = useState(false);

  const setUser = (currentUser: Object) => {
    localStorage.setItem("user", JSON.stringify(currentUser));
    updateUser(currentUser);
  };
  return (
    <Grommet theme={theme} full>
      <ApolloProvider client={client}>
        <UserContext.Provider value={{ user, setUser }}>
          <Router>
            <div>
              <ResponsiveContext.Consumer>
                {(size) => (
                  <Box fill>
                    <AppBar>
                      <Heading level="3" margin="none">
                        My App
                      </Heading>
                      <Button
                        icon={<Notification />}
                        onClick={() => setShowSideBar(!showSidebar)}
                      />
                    </AppBar>
                    <Box
                      direction="row"
                      flex
                      overflow={{ horizontal: "hidden" }}
                    >
                      <Switch>
                        <PrivateRoute exact={true} path="/">
                          <Home />
                        </PrivateRoute>
                        <Route path="/login">
                          <div>Login</div>
                        </Route>
                      </Switch>
                      {!showSidebar || size !== "small" ? (
                        <Collapsible direction="horizontal" open={showSidebar}>
                          <Box
                            flex
                            width="medium"
                            background="light-2"
                            elevation="small"
                            align="center"
                            justify="center"
                          >
                            sidebar
                          </Box>
                        </Collapsible>
                      ) : (
                        <Layer>
                          <Box
                            background="light-2"
                            tag="header"
                            justify="end"
                            align="center"
                            direction="row"
                          >
                            <Button
                              icon={<FormClose />}
                              onClick={() => setShowSideBar(!showSidebar)}
                            />
                          </Box>
                          <Box
                            fill
                            background="light-2"
                            align="center"
                            justify="center"
                          >
                            sidebar
                          </Box>
                        </Layer>
                      )}
                    </Box>
                  </Box>
                )}
              </ResponsiveContext.Consumer>
            </div>
          </Router>
        </UserContext.Provider>
      </ApolloProvider>
    </Grommet>
  );
}

export default App;
