from django.apps import AppConfig


class AtkInventoryConfig(AppConfig):
    name = 'atk_inventory'
