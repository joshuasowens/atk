from django.apps import AppConfig


class AtkAuthConfig(AppConfig):
    name = 'atk_auth'
