import graphene
import atk_auth.schema

class Query(
    graphene.ObjectType,
):
    hello = graphene.String(default_value="Hi!")


class Mutation(
    atk_auth.schema.Mutation,
    graphene.ObjectType,
):
    pass


# class Subscription(
    # graphene.ObjectType,
# ):
    # pass


schema = graphene.Schema(
    query=Query,
    mutation=Mutation,
    # subscription=Subscription
)
