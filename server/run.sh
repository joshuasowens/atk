# abs path to current script
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

python "${SCRIPT_DIR}/startup/startup.py" --env "${ENVIRONMENT}"
