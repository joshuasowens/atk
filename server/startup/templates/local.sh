{% for module_to_migrate in modules_to_migrate %}
  ./manage.py makemigrations {{ module_to_migrate }};
  ./manage.py migrate {{ module_to_migrate }};
{% endfor %}

./manage.py migrate

./manage.py createsuperuser --no-input

./manage.py collectstatic --noinput

./manage.py runserver 0.0.0.0:8000
