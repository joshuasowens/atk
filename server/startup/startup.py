import os
import argparse
from jinja2 import Environment, PackageLoader
from functools import cached_property
from dotenv import load_dotenv, find_dotenv

class Django_Startup:
    def __init__(
            self,
            env,
            port_number=None,
            **kwargs
    ):
        for key, value in { **{ k: v for k, v in locals().items() if k not in ['self', 'kwargs']}, **kwargs }.items():
            setattr(self, f'_{key}', value)

    @classmethod
    def setup_argparser(cls, parser = None):
        parser = argparse.ArgumentParser() if parser is None else parser
        parser.add_argument('--env', action='store', help='The environment to launch the project in')
        return parser

    @classmethod
    def __init_from_args__(cls):
        parser = cls.setup_argparser()
        args = vars(parser.parse_args())
        print(f'{args=}')
        return cls(**args)

    @cached_property
    def jinja_env(self):
        return Environment(loader=PackageLoader('startup', 'templates'))

    @cached_property
    def env(self):
        key = 'env'
        key_input = f'_{key}'
        if getattr(self, key_input, None) is None:
            raise Exception('Must provide a non None value')
        return getattr(self, key_input)



    @cached_property
    def shell_command_template_file(self):
        key = 'shell_command_template_file'
        key_input = f'_{key}'
        if getattr(self, key_input, None) is None:
            return f'{self.env}.sh'
        return getattr(self, key_input)

    @cached_property
    def shell_command_template(self):
        key = 'shell_command_template'
        key_input = f'_{key}'
        if getattr(self, key_input, None) is None:
            return self.jinja_env.get_template(self.shell_command_template_file)
        return getattr(self, key_input)

    @cached_property
    def modules_to_migrate(self):
        key = 'modules_to_migrate'
        key_input = f'_{key}'
        if getattr(self, key_input, None) is None:
            return [
                'auth',
                'inventory',
            ]
        return getattr(self, key_input)

    @cached_property
    def shell_command(self):
        key = 'shell_command'
        key_input = f'_{key}'
        if getattr(self, key_input, None) is None:
            return self.shell_command_template.render(
                modules_to_migrate=self.modules_to_migrate
            )
        return getattr(self, key_input)

    def __call__(self, *args, **kwargs):
        print(f'{self.shell_command=}')
        os.system(self.shell_command)

if __name__ == '__main__':
    load_dotenv(find_dotenv())
    startup = Django_Startup.__init_from_args__()
    startup()
